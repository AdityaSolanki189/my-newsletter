const express = require("express");
const bodyParser = require("body-parser");
const request = require("request");

// using mailchimp library
const client = require("@mailchimp/mailchimp_marketing");

const app = express();

// middlewares
app.use(express.static("public"));
app.use(bodyParser.urlencoded({extended:true}));

client.setConfig({
    apiKey: "fb26a491eeba4e6d722ad552c928a596-us18",
    server: "us18"
});

app.get("/", function(req, res){
    res.sendFile(__dirname+"/signup.html");
}); 

app.post("/", function(req, res){

    const listId = '85ff17110a';
    const subscribingUser = {
        firstName: req.body.first_name,
        lastName: req.body.last_name,
        email: req.body.email  
    };

    console.log(subscribingUser);

    async function addMembers() {
        try{
            const response = await client.lists.addListMember(listId, 
                {
                    email_address: subscribingUser.email,
                    status: "subscribed",
                    merge_fields: {
                        FNAME: subscribingUser.firstName,
                        LNAME: subscribingUser.lastName,
                    }, 
                },  
                { 
                    skipMergeValidation: false
                }
            );

            console.log('====================================');
            console.log(response);
            console.log('====================================');

            res.sendFile(__dirname+"/success.html")
            
        }catch(e){
            res.sendFile(__dirname+"/failure.html")
        }
        // const response = await client.lists.getAllLists();

        // const response = await client.lists.getList(listId);
        
    }
    
    // Checking MailChimps Status
    // async function run() {
    //     const response = await client.ping.get();
    //     console.log(response);
    // } 
    // run();

    addMembers(); 
     
});

app.post("/failure", function(req,res) {
    res.redirect("/");
});

app.listen(process.env.PORT || 3000, function(){
    console.log("Server is running on PORT: 3000");
});

// API Key
// fb26a491eeba4e6d722ad552c928a596-us18

// List Id
// 85ff17110a